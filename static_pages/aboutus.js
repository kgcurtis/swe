$.getJSON("https://gitlab.com/api/v4/projects/8511847/repository/commits?per_page=5000", {}, function(data) {
    $('#commits').html(data.length);
    counter = {}
	for (var i in data) {
		email = data[i].committer_email;
		name = email.substring(0, email.indexOf("@"))
		if (name in counter) {
			counter[name] += 1;
		} else {
			counter[name] = 1;
		}
	}
  counter["shuaixu"] += counter["root"]
  counter["vitolins.edgars"] += counter["edgarsv"]

	for (var i in counter) {
		if (document.getElementById(i)) {
            $('[id=\''+i+'\']').html(counter[i]);
		}
	}
});

$.getJSON("https://gitlab.com/api/v4/projects/8511847/issues", {}, function(data) {
    $('#issues').html(data.length);
});
