import urllib.parse
import copy

from django.test import TestCase
from django.test.client import RequestFactory
from django.conf import settings

from app import views

PAGE_SIZE = settings.REST_FRAMEWORK['PAGE_SIZE']


class AbstractListTest(TestCase):
    fixtures = ['test']

    def setUp(self):
        self.factory = RequestFactory()

    def get_url(self):
        raise NotImplementedError("Abstract class method")

    def make_req_url(self, params):
        return self.get_url() + '?' + urllib.parse.urlencode(params)

    def make_request(self, url):
        raise NotImplementedError("Abstract class method")

    def for_every_page(self, params, f):
        url_params = copy.deepcopy(params)
        url_params['page'] = 1
        next_page = self.make_req_url(url_params)
        while next_page != None:
            response = self.make_request(next_page)
            next_page = response.data['next']
            f(response)

    def check_filter(self, params):
        def check(response):
            for result in response.data['results']:
                for k, v in params.items():
                    self.assertTrue(k in result)
                    self.assertEqual(result[k], v)
        return check

    def check_search(self, search_term):
        def check(response):
            for result in response.data['results']:
                appear = False
                for k, v in result.items():
                    if search_term in str(v):
                        appear = True
                self.assertTrue(appear)
        return check    


class BusinessList(AbstractListTest):
    fixtures = ['test']

    def get_url(self):
        return '/business'

    def make_request(self, url):
        response = self.factory.get(url)
        view = views.BusinessList.as_view()
        return view(response)

    def test_page_limit(self):
        def check_status_and_page_limit(response):
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.data['results']) <= PAGE_SIZE)
        self.for_every_page({}, check_status_and_page_limit)

    def test_filter_bbb_score(self):
        for rating in ['C-', 'C', 'C+', 'B-', 'B', 'B+', 'A-', 'A', 'A+']:
            params = {'bbb_rating': rating}
            self.for_every_page(params, self.check_filter(params))

    def test_filter_business_name(self):
        params = {'name': 'The Beartooth Grill'}
        self.for_every_page(params, self.check_filter(params))

    def test_filter_business_id(self):
        params = {'business_id': '1000026721'}
        self.for_every_page(params, self.check_filter(params))

    def test_filter_state(self):
        params = {'state': 'TX'}
        self.for_every_page(params, self.check_filter(params))

    def test_filter_industry_id(self):
        params = {'industry_id': '72'}
        self.for_every_page(params, self.check_filter(params))

    def test_search(self):
        params = {'search': 'Clothing'}
        self.for_every_page(params, self.check_search(params['search']))

    def test_sort(self):
        sort_by = 'overall_score'

        def check_sorted(response):
            # start off max value, always in order if there is one element
            current = float('inf')
            for result in response.data['results']:
                next_score = float(result[sort_by])
                self.assertTrue(next_score <= current)
                current = next_score
        params = {'sort_by': sort_by}
        self.for_every_page(params, check_sorted)

    def test_filter_invalid_id(self):
        params = {'business_id': '0', 'page': 1}
        response = self.make_request(self.make_req_url(params))
        self.assertEqual(len(response.data['results']), 0)
        self.assertEqual(response.data['next'], None)
        # error message?


class StateList(AbstractListTest):
    fixtures = ['test']

    def get_url(self):
        return '/state'

    def make_request(self, url):
        response = self.factory.get(url)
        view = views.StateList.as_view()
        return view(response)

    def test_page_limit(self):
        def check_status_and_page_limit(response):
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.data['results']) <= PAGE_SIZE)
        self.for_every_page({}, check_status_and_page_limit)

    def test_filter_state_name(self):
        params = {'state_name': 'Texas'}
        self.for_every_page(params, self.check_filter(params))

    def test_filter_state_short_name(self):
        params = {'state_short_name': 'TX'}
        self.for_every_page(params, self.check_filter(params))

    def test_search(self):
        params = {'search': 'Texas'}
        self.for_every_page(params, self.check_search(params['search']))

    def test_sort_descending(self):
        for sort_by in ['gdp', 'num_of_bcorps', 'num_of_aplus_businesses']:
            def check_sorted(response):
                # start off max value, always in order if there is one element
                current = float('inf')
                for result in response.data['results']:
                    self.assertTrue(sort_by in result)
                    next_score = float(result[sort_by])
                    # descending
                    self.assertTrue(next_score <= current)
                    current = next_score
            params = {'sort_by': sort_by}
            self.for_every_page(params, check_sorted)


class IndustryList(AbstractListTest):
    fixtures = ['test']

    def get_url(self):
        return '/industry'

    def make_request(self, url):
        response = self.factory.get(url)
        view = views.IndustryList.as_view()
        return view(response)

    def test_page_limit(self):
        def check_status_and_page_limit(response):
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.data['results']) <= PAGE_SIZE)
        self.for_every_page({}, check_status_and_page_limit)

    def test_filter_industry_name(self):
        params = {'industry_name': 'Accommodation and Food Services'}
        self.for_every_page(params, self.check_filter(params))

    def test_filter_industry_id(self):
        params = {'industry_id': '72'}
        self.for_every_page(params, self.check_filter(params))

    def test_search(self):
        params = {'search': 'Arts'}
        self.for_every_page(params, self.check_search(params['search']))

    def test_sort_descending(self):
        for sort_by in ['num_of_firms', 'gdp']:
            with self.subTest(sort_by=sort_by):
                def check_sorted(response):
                    # start off max value, always in order if there is one element
                    current = float('inf')
                    for result in response.data['results']:
                        self.assertTrue(sort_by in result)
                        next_score = float(result[sort_by])
                        # descending
                        self.assertTrue(next_score <= current)
                        current = next_score
                params = {'sort_by': sort_by}
                self.for_every_page(params, check_sorted)
