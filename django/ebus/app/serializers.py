from rest_framework import serializers
from app.models import Business, State, Industry


class BusinessSerializer(serializers.ModelSerializer):
    industry_name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return obj.industry_id.industry_name

    class Meta:
        model = Business
        fields = '__all__'


class IndustrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Industry
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'
