from django.db import models


class Industry(models.Model):

    industry_id = models.CharField(max_length=20, unique=True)
    industry_name = models.CharField(max_length=200)

    gdp = models.IntegerField()
    description = models.TextField(blank=True)

    num_of_firms = models.IntegerField(blank=True)
    top_scoring_business_id = models.CharField(max_length=20, blank=True)
    top_scoring_business_name = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = 'Industries'


class State(models.Model):

    state_name = models.CharField(max_length=200)
    state_short_name = models.CharField(max_length=20, unique=True)
    gdp = models.IntegerField()

    num_of_bcorps = models.IntegerField(blank=True)
    num_of_aplus_businesses = models.IntegerField(blank=True)
    largest_industry_id = models.CharField(max_length=20, blank=True)
    largest_industry_name = models.CharField(max_length=200, blank=True)
    latitude = models.CharField(max_length=20, blank=True)
    longitude = models.CharField(max_length=20, blank=True)

    class Meta:
        verbose_name_plural = 'States'


class Business(models.Model):

    business_id = models.CharField(max_length=20)
    industry_id = models.ForeignKey(
        Industry, to_field='industry_id', on_delete=models.CASCADE)
    state = models.ForeignKey(
        State, to_field='state_short_name', on_delete=models.CASCADE)
    logo = models.CharField(max_length=200)

    name = models.CharField(max_length=200, blank=True)
    category = models.CharField(max_length=200, blank=True)
    address = models.CharField(max_length=200, blank=True)
    website = models.CharField(max_length=200, blank=True)
    bbb_rating = models.CharField(max_length=20, blank=True)

    latitude = models.CharField(max_length=20, blank=True)
    longitude = models.CharField(max_length=20, blank=True)

    overall_score = models.DecimalField(max_digits=4, decimal_places=1)

    governance_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)
    workers_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)
    community_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)
    environment_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)
    customers_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)
    operations_score = models.DecimalField(
        max_digits=4, decimal_places=1, blank=True)

    class Meta:
        verbose_name_plural = 'Businesses'
