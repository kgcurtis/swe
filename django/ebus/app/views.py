import django_filters.rest_framework
from django.shortcuts import render
from django.http import HttpResponseRedirect
from app.models import Business, State, Industry
from app.serializers import BusinessSerializer, StateSerializer, IndustrySerializer
from rest_framework import generics, filters


def index(request):
    return HttpResponseRedirect("https://documenter.getpostman.com/view/5471337/RWgm3gcT")


class BusinessList(generics.ListCreateAPIView):
    queryset = Business.objects.all().order_by('-overall_score')
    serializer_class = BusinessSerializer
    filter_backends =  (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('business_id', 'bbb_rating',
                     'name', 'state', 'industry_id')
    search_fields = ('name','bbb_rating','website','category', 'industry_id__industry_name','state__state_name','overall_score')

    def get_queryset(self):
        queryset = Business.objects.all()
        sort_by = self.request.query_params.get('sort_by', None)
        if sort_by in {'overall_score'}:
            # negative for descending
            queryset = queryset.order_by('-'+str(sort_by))
        if sort_by in {'name','category','address','bbb_rating','state'}:
            queryset = queryset.order_by(str(sort_by))
        return queryset

class AllBusiness(BusinessList):
    pagination_class = None

class StateList(generics.ListCreateAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('state_short_name', 'state_name')
    search_fields = ('state_name','state_short_name','largest_industry_name','gdp','num_of_aplus_businesses','num_of_bcorps')

    def get_queryset(self):
        queryset = State.objects.all()
        sort_by = self.request.query_params.get('sort_by', None)
        if sort_by in {'gdp', 'num_of_bcorps', 'num_of_aplus_businesses'}:
            # negative for descending
            queryset = queryset.order_by('-'+str(sort_by))
        return queryset

class AllState(StateList):
    pagination_class = None

class IndustryList(generics.ListCreateAPIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('industry_id', 'industry_name')
    search_fields = ('industry_name','top_scoring_business_name','gdp', 'industry_id','num_of_firms')

    def get_queryset(self):
        queryset = Industry.objects.all()
        sort_by = self.request.query_params.get('sort_by', None)
        if sort_by in {'gdp', 'num_of_firms'}:
            # negative for descending
            queryset = queryset.order_by('-'+str(sort_by))
        return queryset

class AllIndustry(IndustryList):
    pagination_class = None

class BusinessDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    lookup_field = "business_id"


class StateDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    lookup_field = "state_short_name"


class IndustryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Industry.objects.all()
    serializer_class = IndustrySerializer
    lookup_field = "industry_id"
