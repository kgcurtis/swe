from django.urls import path
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path("", views.index, name='index'),
    url(r'^all_business/$', views.AllBusiness.as_view()),
    url(r'^business/$', views.BusinessList.as_view()),
    url(r'^business/(?P<business_id>[0-9]+)/$',
        views.BusinessDetail.as_view()),
    url(r'^all_industry/$', views.AllIndustry.as_view()),
    url(r'^industry/$', views.IndustryList.as_view()),
    url(r'^industry/(?P<industry_id>[0-9]+)/$',
        views.IndustryDetail.as_view()),
    url(r'^all_state/$', views.AllState.as_view()),
    url(r'^state/$', views.StateList.as_view()),
    url(r'^state/(?P<state_short_name>[a-zA-Z]+)/$',
        views.StateDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
