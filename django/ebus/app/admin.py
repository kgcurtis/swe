from django.contrib import admin
from django.apps import apps
from app.models import Business, State, Industry

# Register your models here.
# app = apps.get_app_config('app')
# for model_name, model in app.models.items():
#     admin.site.register(model)


class BusinessAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Business._meta.get_fields()]


class IndustryAdmin(admin.ModelAdmin):
    list_display = [
        field.name for field in Industry._meta.get_fields() if field.name != 'business']


class StateAdmin(admin.ModelAdmin):
    list_display = [
        field.name for field in State._meta.get_fields() if field.name != 'business']


admin.site.register(Business, BusinessAdmin)
admin.site.register(Industry, IndustryAdmin)
admin.site.register(State, StateAdmin)
