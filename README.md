# Comments:

We did not use a makefile.
Our backend server is located in django/ebus/app (with the main routing in
django/ebus/app/urls.py, the views logic in django/ebus/app/views.py and the
models defined in django/ebus/app/models.py) with our tests in django/ebus/app/tests.py .
Our Postman tests are located in Postman.json . Our frontend tests are in our
frontend repo (kgcurtis/swe-frontend) with the mocha tests in the test/tests.js
file and the selenium gui tests in the webtests/frontend.py file.
Our visualizations can be found at the bottom of our About Us page.

Name #1: Katherine Curtis
EID #1: kgc499
GitLab ID #1: kgcurtis
Estimated completion time #1: 1
Actual completion time #1: 1


Name #2: Edgars Vitolins
EID #2: ev7847
GitLab ID #2: vitolins.edgars
Estimated completion time #2: 1
Actual completion time #2: 1


Name #3: Benjamin Chen
EID #3: bbc484
GitLab ID #3: ben12143chen
Estimated completion time #3: 1
Actual completion time #3: 1


Name #4: Shuai Xu
EID #4: sx2426
GitLab ID #4: shuaixu1997
Estimated completion time #4: 3
Actual completion time #4: 2


Name #5: Ryan Rice
EID #5: rtr626
GitLab ID #5: ryanr1230
Estimated completion time #5: 1
Actual completion time #5: 1


# GitLab pipelines:

https://gitlab.com/kgcurtis/swe/pipelines
https://gitlab.com/kgcurtis/swe-frontend/pipelines

# Website:

http://ethicalbusinesses.me/

# Postman Documentation:

http://api.ethicalbusinesses.me/


# Git SHA:

8d17fb84018e37ada2728795e752dac50dbf103e

