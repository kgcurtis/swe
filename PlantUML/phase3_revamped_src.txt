@startuml
!define Table(name,desc) class name as "desc" << (T,#FFAAAA) >>
' we use bold for primary key
' green color for unique
' and underscore for not_null
!define primary_key(x) <b>x</b>
!define unique(x) <color:green>x</color>
!define not_null(x) <u>x</u>
' other tags available:
' <i></i>
' <back:COLOR></color>, where color is a color name or html color code
' (#FFAACC)
' see: http://plantuml.com/classes.html#More
' entities

Table(business, "Business") {
primary_key(business_id) INTEGER
industry_id FKEY 
state FKEY
logo INTEGER
{field} name VARCHAR(200)
{field} category VARCHAR(200)
{field} address VARCHAR(200)
{field} accredidiation_date DATE
{field} website VARCHAR(200)
{field} bbb_rating VARCHAR(20)
{field} overall_score DECIMAL(5,1)
{field} governance_score DECIMAL(5,1)
{field} workers_score DECIMAL(5,1)
{field} community_score DECIMAL(5,1)
{field} environment_score DECIMAL(5,1)
{field} customers_score DECIMAL(5,1)
{field} operations_score DECIMAL(5,1)
{field} latitude VARCHAR(20)
{field} longitude VARCHAR(20)

}

Table(state, "State") {
{field} primary_key(state_name) VARCHAR(200)
{field} state_short_name VARCHAR(20)
{field} gdp_in_millions INTEGER
{field} num_of_bcorps INTEGER
{field} num_of_aplus_businesses INTEGER
{field} largest_industry_id VARCHAR(20)
{field} latitude VARCHAR(20)
{field} longitude VARCHAR(20)
}

Table(industry, "Industry") {
{field} primary_key(industry_id) VARCHAR(20)
{field} industry_name VARCHAR(200)
{field} gdp_in_millions INTEGER
{field} description TEXT
{field} num_of_firms INTEGER
{field} top_scoring_business_id VARCHAR(20)

}


' Notes 
' Need to maybe add in foreign keys, is primary key ID? 
business "1..N" --* "1" industry : A business classifies in one industry
business "1..N" -- "1" state : A business resides in one state
state "1..N" *-- "1..N" industry : Each state has its largest industry
' change integer to bigint
' need to include compositive / associate aggregations
' clarify one to one and one ot many
@enduml



